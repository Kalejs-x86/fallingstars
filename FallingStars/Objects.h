#pragma once
#include "stdafx.h"

typedef unsigned char byte;

struct Point
{
public:
	unsigned X, Y;

	Point() {
		X = Y = -1; // Will loop around to bogus value, hopefully cause an error somewhere
	}

	Point(unsigned X_, unsigned Y_){
		X = X_;
		Y = Y_;
	}
};

struct Color_t {
	byte R, G, B;
	Color_t() { R = G = B = 0; }
	Color_t(byte r, byte g, byte b)
	{
		R = r;
		G = g;
		B = b;
	}

	Color_t &operator+(const Color_t& other)
	{
		// OK if overlaps, then it will rotate back to 0
		R += other.R;
		G += other.G;
		B += other.B;
		return *this;
	}
};

class LivePixel_t
{
public:
	Color_t Color; // Color
	Point Position; // Coordinates
	bool VecRight, VecDown; // Movement vectors
	byte Speed;
	bool Dropping; // When static pixels are hit, they will first fall to the floor
	unsigned FallTimeout; // How many ticks left until it can move

	static const unsigned DEFAULT_TIMEOUT = 1; // Value to reset FallTimeout to

	LivePixel_t()
	{
		Color = Color_t();
		Position = Point(0, 0);
		VecRight = VecDown = false;
		Dropping = false;
		FallTimeout = 0;
		Speed = 1;//std::rand() % 3 + 1;
	}
};

class PixelMap
{
private:
	// Static pixel data in a format which can be used by the renderer
	sf::Uint8 *StaticPixels;
	// Moving pixels
	std::vector<LivePixel_t*> LivePixels;
	unsigned WindowWidth, WindowHeight;

	unsigned CoordinateToIndex(const Point &Coordinate)
	{
		if (Coordinate.X > WindowWidth || Coordinate.Y > WindowHeight)
			exit(0xDAAD);
		return (4 * (Coordinate.X + Coordinate.Y * WindowWidth));
	}
public:
	PixelMap() {
		//StaticPixels = nullptr;
		WindowWidth = WindowHeight = 0;
	}

	PixelMap(unsigned Width, unsigned Height)
	{
		StaticPixels = new sf::Uint8[Width * Height * 4];
		WindowWidth = Width;
		WindowHeight = Height;

		for (int x = 0; x < Width; ++x)
		{
			for (int y = 0; y < Height; ++y)
			{
				SetPixelColor(Point(x, y), Color_t(0, 0, 0)); // Initialize with black StaticPixels
			}
		}
	}

	~PixelMap()
	{
		delete StaticPixels;
	}

	Color_t PixelAt(const Point &Coordinate)
	{
		Color_t PixelColor;
		unsigned Index = CoordinateToIndex(Coordinate);
		PixelColor.R = StaticPixels[Index];
		PixelColor.G = StaticPixels[Index + 1];
		PixelColor.B = StaticPixels[Index + 2];
		return PixelColor;
	}

	void SetPixelColor(const Point &Coordinate, const Color_t &Color)
	{
		unsigned Index = CoordinateToIndex(Coordinate);
		StaticPixels[Index] = Color.R;
		StaticPixels[Index + 1] = Color.G;
		StaticPixels[Index + 2] = Color.B;
		StaticPixels[Index + 3] = 255;
	}

	void MovePixel(const Point &Old, const Point &New)
	{
		Color_t PixelColor = PixelAt(Old);
		SetPixelColor(Old, Color_t(0, 0, 0));
		SetPixelColor(New, PixelColor);
	}

	void MakeLivePixel(const Point &PixelPos, Color_t Color, bool InitialPixel = false)
	{
		LivePixel_t *NewPixel = new LivePixel_t();
		NewPixel->Color = Color;
		NewPixel->Position = PixelPos; // New pixel's position is current pixel's predicted position
		NewPixel->VecDown = true;
		NewPixel->VecRight = std::rand() % 2;
		NewPixel->Dropping = !InitialPixel;
		LivePixels.push_back(NewPixel);
	}

	void MakeLivePixel(const Point &PixelPos)
	{
		Color_t PixelColor = PixelAt(PixelPos);
		MakeLivePixel(PixelPos, PixelColor);
		SetPixelColor(PixelPos, Color_t(0, 0, 0));
	}

	sf::Uint8 *GetPixelArray() {
		unsigned long ElementCount = WindowWidth * WindowHeight * 4;
		sf::Uint8 *AllPixels = new sf::Uint8[ElementCount];

		// Load static pixel data
		memcpy_s(AllPixels, ElementCount, StaticPixels, ElementCount);

		for (LivePixel_t *Pixel : LivePixels)
		{
			// Combine live and static pixel data
			unsigned Index = CoordinateToIndex(Pixel->Position);
			AllPixels[Index] = Pixel->Color.R;
			AllPixels[Index + 1] = Pixel->Color.G;
			AllPixels[Index + 2] = Pixel->Color.B;
			AllPixels[Index + 3] = 255;
		}
		return AllPixels; // return result
	}

	std::vector<LivePixel_t*> GetLivePixels() {
		return LivePixels;
	}
};