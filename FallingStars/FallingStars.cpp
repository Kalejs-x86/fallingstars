#include "stdafx.h"
#include "Objects.h"
#include "Rainbow.h"

unsigned WINDOW_WIDTH = -1;
unsigned WINDOW_HEIGHT = -1;

// Stationary pixel wall information
unsigned WALL_HEIGHT = -1;
unsigned WALL_WIDTH = -1;

PixelMap *Screen = nullptr;

void RenderGraphics(sf::RenderWindow *pWindow)
{
	sf::Uint8 *Pixels = Screen->GetPixelArray();

	sf::Image Image;
	Image.create(WINDOW_WIDTH, WINDOW_HEIGHT, Pixels);

	sf::Texture Texture;
	Texture.loadFromImage(Image);

	sf::Sprite Sprite;
	Sprite.setTexture(Texture, true);

	pWindow->draw(Sprite);
	pWindow->display();
	delete Pixels; // Delete copy of pixel array to prevent memory leak
}

void Think()
{
	// Only process moving, non stationary pixels
	auto LivePixels = Screen->GetLivePixels();
	int VecSize = LivePixels.size();
	for (int it = 0; it < VecSize; ++it)
	{
		LivePixel_t *Pixel = LivePixels[it];
		// Calculate new coordinates after movement is applied
		Point OldPos = Pixel->Position;
		Point NewPos = OldPos;

		if (!Pixel->Dropping)
		{
			if (Pixel->VecRight) NewPos.X += Pixel->Speed;
			else NewPos.X -= Pixel->Speed;
		}
		if (Pixel->VecDown)
		{
			if (Pixel->Dropping)
			{
				if (Pixel->FallTimeout > 0)
					Pixel->FallTimeout--;
				else
				{
					Pixel->FallTimeout = LivePixel_t::DEFAULT_TIMEOUT;
					NewPos.Y++;
				}
			}
			else
				NewPos.Y += Pixel->Speed;
		}
		else NewPos.Y -= Pixel->Speed;

		// Check for out of bounds
		if (NewPos.X >= WINDOW_WIDTH || NewPos.X < 0)
		{
			NewPos.X = OldPos.X;
			Pixel->VecRight = !Pixel->VecRight;
		}
		if (NewPos.Y >= WINDOW_HEIGHT || NewPos.Y < 0)
		{
			NewPos.Y = OldPos.Y;
			Pixel->VecDown = !Pixel->VecDown;
			Pixel->Dropping = false;
		}
		
		// Ignore collision if pixel is dropping
		if (!Pixel->Dropping)
		{
			// To prevent diagonal collision, only look at the Y axis
			Point TargetPos = NewPos;
			TargetPos.X = OldPos.X;
			Color_t PixCol = Screen->PixelAt(TargetPos); // What color pixel is at this position?
			if ((PixCol.R == 0 && PixCol.G == 0 && PixCol.B == 0) == false) // Not a black pixel
			{
				// Create new live pixel from the colliding pixel
				Screen->MakeLivePixel(TargetPos);

				// Keep old position for current pixel but change the vector
				NewPos = OldPos;
				Pixel->VecDown = !Pixel->VecDown;
			}
		}

		Pixel->Position = NewPos; // Update stored pixel position
	}
}

void CreateObjects()
{
	// Fill the screen with colors to be 'attacked'
	for (int x = 0; x < WALL_WIDTH; ++x)
	{
		Color_t Color = Rainbow();
		for (int y = 0; y < WALL_HEIGHT / 4 * 3; ++y)
		{
			Screen->SetPixelColor(Point(x, y), Color);
		}
	}

	// Create the first moving pixel
	Screen->MakeLivePixel(Point(WINDOW_WIDTH / 2, WINDOW_HEIGHT - 5), Color_t(255, 255, 255), true);
}

void Cleanup()
{
	delete Screen;
}

int main(int argc, char **argv)
{
	if (argc == 3) // Executable name + Width + Height
	{
		if (argv[1][0] == '-' || argv[2][0] == '-')
		{
			std::cout << "Resolution input must be an unsigned number";
			return -2;
		}

		try {
			WINDOW_WIDTH = std::stoi(argv[1]);
			WINDOW_HEIGHT = std::stoi(argv[2]);
		}
		catch (std::invalid_argument ex)
		{
			std::cout << "Resolution input must be a number"; return -1;
		}
		catch (std::out_of_range ex)
		{
			std::cout << "Resolution input must be within the limits of an unsigned 16-bit integer"; return -1;
		}

		if (WINDOW_WIDTH < 4 || WINDOW_HEIGHT < 4)
		{
			std::cout << "Window Width and Window Height must be atleast 4";
			return -2;
		}
	}
	else if (argc == 1) // Executable name
	{
		WINDOW_WIDTH = 800;
		WINDOW_HEIGHT = 600;
	}
	else // Anything in between
	{
		std::cout << "Usage: FallingStars [Window Width] [Window Height]\n";
		std::cout << "Default resolution is 800x600";
		return 0;
	}

	WALL_WIDTH = WINDOW_WIDTH;
	WALL_HEIGHT = WINDOW_HEIGHT / 2;

	std::srand(time(0));
	sf::RenderWindow Window;
	Window.create(sf::VideoMode(WINDOW_WIDTH, WINDOW_HEIGHT), "Falling Stars");
	Window.setFramerateLimit(120);

	Screen = new PixelMap(WINDOW_WIDTH, WINDOW_HEIGHT);
	CreateObjects();

	while (Window.isOpen())
	{
		sf::Event e;
		while (Window.pollEvent(e))
		{
			if (e.type == sf::Event::Closed)
				Window.close();
			if (e.type == sf::Event::KeyPressed)
			{
				if (e.key.code == sf::Keyboard::A)
					Window.setFramerateLimit(5);
				if (e.key.code == sf::Keyboard::S)
					Window.setFramerateLimit(20);
				if (e.key.code == sf::Keyboard::D)
					Window.setFramerateLimit(60);
				if (e.key.code == sf::Keyboard::F)
					Window.setFramerateLimit(120);
			}
		}
		Think();
		RenderGraphics(&Window);
	}

	Cleanup();
    return 0;
}

