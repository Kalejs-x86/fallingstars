# Falling Stars
A little challenge I made for myself, inspired by an animation I found on the internet.
For graphics rendering I used SFML 2.5.1.

### Original

![Original](img/original.gif)

### My attempt

![Result](img/result.gif)

## Usage

Download and extract the compiled binary.

Run FallingStars.exe without any parameters or by specifying the window resolution like so

```
FallingStars 1280 800
```

Default resolution is 800x600

